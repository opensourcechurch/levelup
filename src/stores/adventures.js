import { defineStore } from 'pinia';

import {ref, computed, watch } from 'vue'
import { SB } from "../db"
import _ from 'lodash';
import { useAuthStore } from '../stores/auth'
import { useQuasar } from 'quasar'

console.log("INIT PINIA::ADVENTURES STORE")

export const useAdventuresStore = defineStore('adventures', () => {

  const auth = useAuthStore()
  const $q = useQuasar()

  const notify_error = (message, caption="") => $q.notify({
    message: message,
    color: "negative",
    icon: "error",
    caption: caption
  })

  /*
  ADVENTURES
  */

  const _adventures = ref([])

  // LIST

  // TODO: séparer la colonne 'data' d'une nouvelle colonne 'meta', parce que là quand on télécharge l'apperçu
  // de l'aventure, ça prend tout le contenu (sauf les quêtes, mais ça fait déjà beaucoup de texte.)
  const list = async () => {
    // On recherche pas si on a déjé
    if (_adventures.value.length) return
    // On liste toutes les aventures
    console.log("Fetching Adventures:")
    const { data } = await SB
    .from("lu_adventures")
    .select("id, name, data, description, private, slug, owners:lu_users(id, full_name, avatar_url), admin:lu_adventures_admin(official)")
    _adventures.value = data
    console.log("Adventures:", data.length)
  }

  /*
    Retourne un aventure, soit par id soit pas slug
  */
  var _is_getting = []
  const get_adventure = async (a = { id: false, slug: false}) => {
    // From cache
    var adventure = adventures.value.find(_a => _a.id == a.id || _a.slug == a.slug)
    if (adventure) return adventure
    if (a.id && _is_getting.includes(a.id)) return // On a déjà demandé
    console.log("Fetching Adventure:", a.id || a.slug)
    if (a.id) _is_getting.push(a.id)
    // Query DB
    const { data } = await SB.from("lu_adventures")
    .select("id, name, data, description, private, slug, owners:lu_users(id, full_name, avatar_url), admin:lu_adventures_admin(official)")
    .eq(a.id ? "id" : "slug", a.id ? a.id : a.slug)
    .single()
    if (data) {
      // On remplace l'aventure dans la liste
      _adventures.value = adventures.value.filter(a => a.id != data.id)
      _adventures.value.push(data)
      return data
    }
    console.log("Adventures:", _adventures.value.length)
  }

  const adventures = computed(() => {
    return _adventures.value.map(a => {
      // Des trucs utiles à rajouter à l'aventure
      a.official = a.admin && a.admin.official
      return a
    })
  })

  // CREATE

  const create_adventure = async() => {
    console.log("Creating adventure for user", auth.uid)
    if (!auth.logged) return
    const {error} = await SB.from("lu_adventures").insert({admin: auth.uid})
    if (error) {
      notify_error(error.message, "create_adventure")
      return false
    }
  }

  const delete_adventure = async (adventure_id) => {
    const { data, error } = await SB
    .from('lu_adventures')
    .delete().eq("id", adventure_id)
    console.log(data, error)
    adventures.value = adventures.value.filter(a => a.id != adventure_id)
  }

  // TAGS

  const adventure_tags = ref([])
  const get_adventures_tags = async () => {
    const { data, error } = await SB.rpc('get_adventures_tags')
    if (error) {
      notify_error(error.message, "get_adventures_tags")
      return false
    }
    adventure_tags.value = data
  }


  /*
  SPECTIFIC ADVENTURE
  */

 const _adventure = ref({})  // the last get_adventure

  // Retourne une aventure en cherchant soit par id (défaut) soit par slug
  const set_adventure = async (a = { id: false, slug: false}) => {
    console.log("Fetching Full Adventure:", a.id || a.slug)
    // Si on change d'aventure, on met à zéro
    if ((a.id && a.id != adventure.value.id) || (a.slug && a.slug != _adventure.value.slug)) _adventure.value = {}
    const { data, error } = await SB
    .from("lu_adventures")
    .select("*, quests:lu_quests (*), owners:lu_users (*)")
    .eq(a.id ? "id" : "slug", a.id ? a.id : a.slug)
    .single()
    if (error) {
      console.log("Adventure error", error)
      _adventure.value = { error: "Unexisting"}
    }
    if (data) {
      _adventure.value = data
      console.log("Current adventure:", data.name)
    }
    return data
  }

  const update_adventure = async(a) => {
    if (!adventure_loaded.value) return
    var a = _.cloneDeep(a)
    const { data, error } = await SB
    .from('lu_adventures')
    .update({
      slug: a.slug,
      name: a.name,
      description: a.description,
      private: a.private,
      data: a.data,
      steps: a.steps
    })
    .eq("id", adventure.value.id)
    if (error) {
      notify_error(error.message, "update_adventure")
      return false
    }
    // Update course
    set_adventure({id: adventure.value.id})
    return true
  }

  const update_adventure_info = async(info) => {
    if (!adventure_loaded.value) return
    const { data, error } = await SB
    .from('lu_adventures')
    .update({slug: info.slug, name: info.name, description: info.description, private: info.private})
    .eq("id", adventure.value.id)
    if (error) {
      notify_error(error.message, "update_adventure_info")
      return false
    }
    // Update course
    set_adventure({id: adventure.value.id})
    return true
  }

  const update_adventure_data = async(d) => {
    console.log("Updating adventure data")
    const { data, error } = await SB.from('lu_adventures').update({data: d}).eq("id", adventure.value.id)
    console.log(data, error)
    if (error) {
      notify_error(error.message, "update_adventure_data")
      return false
    }
    // Update course
    set_adventure({id: adventure.value.id})
    return true
  }

  const create_quest = async (info) => {
    if (!adventure_loaded.value) return
    const { data, error } = await SB
    .from('lu_quests')
    .insert({adventure: adventure.value.id})
    .select().single()
    // Update course
    set_adventure({id: adventure.value.id})
    return data
  }

  const delete_quest = async (quest_id) => {
    const { data, error } = await SB
    .from('lu_quests')
    .delete().eq("id", quest_id)
    console.log(data, error)
    set_adventure({id: adventure.value.id})
  }

  const update_quest = async (info) => {
    if (!adventure_loaded.value) return
    const { data, error } = await SB
    .from('lu_quests')
    .update({id: info.id, type: info.type, data: info.data, xp: info.xp, name: info.name, side: info.side, description: info.description})
    .eq("id", info.id)
    console.log(data, error)
    if (error) {
      notify_error(error.message, "update_adventure_data")
      return false
    }
    // Update course
    set_adventure({id: adventure.value.id})
    return true
  }

  // Computed props

  const adventure_loaded = computed(() => !_.isEmpty(_adventure.value))
  const is_owner = computed(() => adventure_loaded.value && adventure.value.owners.find(o => o.id == auth.uid) != undefined)

  // LOGS

  const user_logs = ref([])

  const fetch_user_logs = async () => {
    if (!auth.logged) return
    console.log("Fetching Logs for user:", auth.uid)
    const { data } = await SB
    .from("lu_logs")
    .select("*, logs:lu_logs(id, finished, data, quest, created_at)")
    .eq("user", auth.uid)
    .is("adventure_log", null)
    .order('created_at', { ascending: false })
    .order('created_at', { foreignTable:"lu_logs", ascending: true })
    user_logs.value = data
    console.log("User logs: ", data.length)
  }

  const log_adventure_started = async (adventure_id) => {
    if (!auth.logged) return
    console.log("STARTING ADVENTURE")
    console.log(auth.uid)
    const { data, error } = await SB
    .from('lu_logs')
    .insert({user: auth.uid, adventure: adventure_id, started: true})
    console.log(data, error)
    if (error) {
      notify_error(error.message, "log_adventure_started")
      return false
    }
    fetch_user_logs()
  }

  /*
    Log un quête, avec data (text, don), et status (finished).
  */
  const log_quest = async (quest_id, data = null, status = true, log_id = false) => {
    var adventure_id = adventure.value.id
    if (!auth.logged || !adventure_loaded.value || !adventure_log(adventure_id)) return
    // Est-ce qu'un log existe déjà pour cette quête?
    if (log_id) { // on update
      const r = await SB
      .from('lu_logs')
      .update({finished: status, data: data})
      .eq("id", log_id)
      console.log(r)
    } else { // on crée un log
      const r = await SB
      .from('lu_logs')
      .insert({user: auth.uid, adventure: adventure_id, adventure_log: adventure.value.progress.id, quest: quest_id, finished: status, data: data})
      console.log(r)
    }
    // Update logs
    fetch_user_logs()
  }

  /*
    Supprime un log de quête spécifique.
  */
  const log_quest_remove = async(log_id) => {
    const { data, error } = await SB
      .from('lu_logs')
      .delete()
      .eq("id", log_id)
      console.log(data, error)
    // Update logs
    fetch_user_logs()
  }

  /*
    Supprime tous les logs de quête de quest_id pour l'aventure en cours.
  */
  const log_quest_clear = async(quest_id) => {
    console.log("CLEAR", quest_id, adventure.value.progress.id)
    const { data, error } = await SB
      .from('lu_logs')
      .delete()
      .eq("quest", quest_id)
      .eq("user", auth.uid)
      .eq("adventure_log", adventure.value.progress.id)
      console.log(data, error)
    // Update logs
    fetch_user_logs()
  }

  // Dernier log pour l'aventure en cours
  const adventure_log = (adventure_id) => {
    return user_logs.value.find(p => p.adventure == adventure_id && p.started && !p.quest)
  }
  /*
    Adventure Progress

    Le progres pour l'aventure en cours

  */
  const _adventure_progress = computed(() => {
    if (!auth.logged || !adventure_loaded.value) return false
    var log = _.cloneDeep(adventure_log(_adventure.value.id))

    if (log) {
      // on ajoute les quêtes
      var quests = _.cloneDeep(_adventure.value.quests).map(q => {
        var _logs = log.logs.filter(l => l.quest == q.id) || []
        return {
          id: q.id,
          side: q.side,
          xp: q.xp,
          adventure: {
            started: log.started,
            finished: log.finished,
          },
          logs: _logs.map(_l => ({
            done: _l.finished || false,
            data: _l.data || {},
            id: _l.id,
            time: _l.created_at
          })),
          done: q.data.multiple ? _logs.filter(_l => _l.finished).length >= q.data.multiple_min : _logs.filter(_l => _l.finished).length > 0,
          done_number: _logs.filter(_l => _l.finished).length
        }
      })
      log.quests = Object.fromEntries(quests.map(q => [q.id, q]))

      // Peut-être que la quête a été modifiée, et qu'il y a plus d'étapes que dans le log
      while(_adventure.value.steps.length > log.data.steps.length)
        log.data.steps.push(({}))

      // On update le status des quêtes
      var previous_step_done = true
      var next_found = false
      _adventure.value.steps.forEach((s, k) => {
        s.items.filter(i => i.type == "quest").forEach(q => {
          var quest = log.quests[q.id]
          if (quest) { // Peut-être que la quête a été supprimée?
            quest.step = k
             // Est-ce que la quête est ouverte? (soit l'aventure n'est pas sequentielle, soit l'étape précédente est faite)
            quest.open = !_adventure.value.data.sequential || previous_step_done
            if (!next_found && !quest.done && !quest.side) {
              quest.next = true // indique qu'il s'agit de la quête suivante à accomplir
              next_found = true
            } else quest.next = false
          }
        })
        previous_step_done = log.data.steps[k].done
        log.data.steps[k].open = k == 0 || !_adventure.value.data.sequential || log.data.steps[k-1].done
      })
      // on update le progres par étape
      log.steps = log.data.steps
      // Est-ce que toutes les quêtes sont validées?
      log.done = log.data.done
      return log
    }
    else return false
    // {
    //   steps: _adventure.value.data.steps.map(s => ({})),
    //   data: {},
    //   empty: true
    // }
  })

  const adventure = computed(() => {
    if (adventure_loaded.value) {
      // On prépare l'aventure
      var adventure = _.cloneDeep(_adventure.value)

      // On rajoute des trucs utiles
      adventure.is_owner = auth.logged && adventure.owners.map(o => o.id).includes(auth.uid)

      // On ajoute le progres
      adventure.progress = _adventure_progress.value

      return adventure
    }
    else return {
      quests: []
    }
  })

  // USER
  // FIXME: c'est un peu le bordel toussa. Faudrait faire de l'ordre

  const user_info = ref({})

  /*
  Un objet user avec tous les trucs utiles pour l'user en cours
  */
  const user = computed(() => {
    // Pas encore chargé
    if (_.isEmpty(user_info.value))
      return {
        progress: {},
        adventures: []
      }

    else
      var user = _.cloneDeep(user_info.value)

    // On map les aventures
    user.adventures = user_info.value.adventures
      .map(a => {
        get_adventure({ id: a.id}) // on demande de charger l'aventure
        return adventures.value.find(_a => _a.id == a.id)
      })
      .filter(a => a) // les aventures sont peut-être pas encore chargées

    // On rajoute les logs et le progress
    user.logs = user_logs.value
    user.progress = user_progress.value

    return user

  })

  // Progrès par aventure
  // Retourne un objets {aventure_id: log} avec le dernier log par aventure
  const user_progress = computed(() => {
    // unique adventure_id
    var ids = user_logs.value.map(p => p.adventure)
    ids = [... new Set(ids)]
    var p = Object.fromEntries(ids.map(id => [id, user_logs.value.find(p => p.adventure == id)]))
    return p
  })

  // Les aventures en cours pour l'user
  const adventures_in_progress = computed(() => auth.logged ? user_logs.value.filter(l => !l.data.done).map(l=>l.adventure) : [])

  /*
    Choses à faie quand un user se log
  */
  const update_user_info = async () => {
    console.log("Updating user info")
    var uid = auth.uid
    const { data } = await SB.from("lu_users")
    .select("*, adventures:lu_adventures (id)")
    .order('created_at', { foreignTable: 'lu_adventures', ascending: false })
    .eq("id", uid).single()
    user_info.value = data
  }
  watch(() => auth.logged, async (val) => {
    console.log("LOGGGED", val)
    auth.is_admin() // On log si admin ou pas
    // Update lu_users infos
    if (val) {
      const {data, error} = await SB.from("lu_users").upsert({
        id: auth.uid,
        full_name: auth.user.user_metadata.full_name,
        avatar_url: auth.user.user_metadata.avatar_url
      })
    }
    // Get logs
    fetch_user_logs()
    // Update and subscribe to user info
    if (val) {
      var uid = auth.uid
      update_user_info()
      SB.channel('any')
      .on('postgres_changes', { event: '*', schema: 'public', table: `lu_users`, filter:`id=eq.${uid}` }, update_user_info)
      .on('postgres_changes', { event: '*', schema: 'public', table: `lu_adventures`, filter:`admin=eq.${uid}` }, update_user_info)
      .subscribe()
    } else { SB.removeAllChannels() }
  }, {immediate: true})


  /*
  BUCKETS
  */

  const upload_adventure_cover = async (adventure_id, file) => {
    console.log("UPLOAD", adventure_id, file)
    return await SB.storage
    .from('lu_images')
    .upload(`lu_adventures/cover_${adventure_id}`, file, {})
  }

  return {
    adventures, list, create_adventure,
    adventure, get_adventure, set_adventure, is_owner, delete_adventure,
    // Computed
    adventure_loaded,
    // Functions
    create_quest, delete_quest,
    update_adventure, update_quest,
    log_adventure_started,
    log_quest, log_quest_remove, log_quest_clear,
    upload_adventure_cover,
    // Tags
    adventure_tags, get_adventures_tags,
    // Progress
    user, user_logs, user_progress,
    user_info, adventures_in_progress
  }
})

