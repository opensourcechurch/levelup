import { defineStore } from 'pinia';

import {ref, computed, watch } from 'vue'
import { SB } from "../db"


console.log("INIT PINIA::VOIES STORE")

export const useVoiesStores = defineStore('voies', () => {

  const voies = []

  voies.push({
    name: "La voie des Mentors",
    description: "Mieux aider d'autres à progresser dans leurs aventures",
    tag: "Mentor",
    image: "https://upload.wikimedia.org/wikipedia/commons/thumb/e/ea/GANDALF.jpg/548px-GANDALF.jpg",
    ref: "CC-BY-SA Nidoart"
  })

  voies.push({
    name: "La voie des Pingouins",
    description: "Mieux vivre en communauté et collaborer",
    tag: "Pingouins",
    image: "https://image.lexica.art/full_jpg/08c3aa16-7d6d-49ae-af50-7e2174482949",
    ref: "[Lexica Art](https://lexica.art/prompt/4e966b3a-d1d4-47f8-951a-8acd1986eef3)"
  })

  voies.push({
    name: "La voie des Druides",
    description: "Vivre en harmonie avec l'ensemble du vivant",
    tag: "Druide",
    image: "https://art.ngfiles.com/images/870000/870174_artdeepmind_dead-druid.jpg?f1554711222",
    ref: "[CC-BY-NC-ND ArtDeepMind](https://www.newgrounds.com/art/view/artdeepmind/dead-druid)"
  })

  voies.push({
    name: "La voie des Amoureux",
    description: "Grandir en amour dans ses relations de couple",
    tag: "Amoureux",
    image: "https://p0.pxfuel.com/preview/80/920/883/fantasy-love-couple-romance.jpg", //"https://image.lexica.art/full_jpg/156ff855-01f3-4700-b28e-29392d017f79",
    ref: "https://www.pxfuel.com/"
  })

  voies.push({
    name: "La voie des Disciples",
    description: "Avancer à la suite du Christ",
    tag: "Disciple",
    image: "https://image.lexica.art/full_jpg/c169f923-099d-4141-a39f-9b0a2b8ad9ee",
    ref: "[Lexica Art](https://lexica.art/prompt/18ce26ba-afdd-41cb-879b-d36b50eb3de6)"
  })

  const badges = [
    { id: "tuto", icon: "start", name:"Curiosité", description: "Tuto terminé" },
    { id: "100XP", icon: "pets", name:"Persévérance", description: "100 XP gagnés" },
    { id: "1000XP", icon: "grass", name:"Expertise", description: "1000 XP gagnés" },
    { id: "10quests", icon: "park", name:"", description: "10 quêtes terminées" },
    { id: "100quests", icon: "forest", name:"", description: "100 quêtes terminées" },
    { id: "10sidequests", icon: "route", name:"Vagabond·e", description: "10 quêtes annexes terminées" },
    { id: "100sidequests", icon: "hiking", name:"Grand-Pas", description: "100 quêtes annexes terminées" },
    { id: "1adv", icon: "edit", name:"Création", description: "1 aventure crée (avec au moins 2 étapes et 5 quêtes)" },
    { id: "1pubadv", icon: "publish", name:"", description: "1 aventure publique crée" },
    { id: "5pubadv", icon: "construction", name:"Builder", description: "5 aventures publiques crées" },
    { id: "1officialadv", icon: "local_police", name:"", description: "1 aventure officielle crée" }
  ]

  return { voies, badges }
})
