import { defineStore } from 'pinia';

import {ref, computed, watch } from 'vue'
import { SB } from "../db"


console.log("INIT PINIA::AUTH STORE")

export const useAuthStore = defineStore('auth', () => {
  const session = ref(null)
  const orgas = ref({a:1})

  const user = computed(() => session.value ? session.value.user : {})
  const uid = computed(() => user.value.id)
  const logged = computed(() => session.value ? true : false)
  const admin = ref(false)
  const avatar = computed(() => user.value.user_metadata.avatar_url)
  const name = computed(() => user.value.user_metadata.full_name)
  const pseudo = computed(() => user.value.user_metadata.name)

  const signInWithDiscord = async () => {
    // var URL = window.location.origin + "/#/user"
    var URL = window.location.href
    // return
    const { data, error } = await SB.auth.signInWithOAuth({
      provider: 'discord',
      options: {
        redirectTo: URL
      }
    })
  }

  const is_admin = async () => {
    if (!logged.value) return false
    // On recheck pas si on est déjà admin
    else if (admin.value) return true
    // Dans le doute, on check
    else {
      const { data, error } = await SB.rpc('is_admin', { id: uid.value })
      console.log("Is admin?", data)
      admin.value = data
      return data
    }
  }

  return {user, orgas, logged, session, uid, admin, is_admin, avatar, name, pseudo, signInWithDiscord}
})
