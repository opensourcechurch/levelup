import { route } from 'quasar/wrappers'
import { createRouter, createMemoryHistory, createWebHistory, createWebHashHistory } from 'vue-router'
import routes from './routes'
import { useAuthStore } from '../stores/auth'

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

export default route(function (/* { store, ssrContext } */) {
  const createHistory = process.env.SERVER
    ? createMemoryHistory
    : (process.env.VUE_ROUTER_MODE === 'history' ? createWebHistory : createWebHashHistory)

  const Router = createRouter({
    scrollBehavior: (to, from, savedPosition) => {
      // Keep position when changing step in adventure
      if (to.name == from.name && to.name == "AdventuresView" && to.params.adventure_slug == from.params.adventure_slug
          && !history.state.to_top)
        return {}
      return { left: 0, top: 0 }
    },
    routes,

    // Leave this as is and make changes in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    history: createHistory(process.env.VUE_ROUTER_BASE)
  })

  // https://router.vuejs.org/api/interfaces/Router.html#Methods-beforeResolve

  Router.beforeResolve((to, from) => {
    const auth = useAuthStore()
    if (to.meta.requires_auth && !auth.logged) return false
    if (to.meta.requires_admin && (!auth.logged || !auth.admin)) return false
  })

  return Router
})
