
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', name: "Home", component: () => import('pages/IndexPage.vue'), props: true },
      { path: 'login', name: "UserLogin", component: () => import('pages/UserLogin.vue') }
    ]
  },
  {
    path: '/aventures',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', name: "AdventuresHome", component: () => import('pages/AdventuresHome.vue') },
      { path: ':adventure_slug/:step?', name: "AdventuresView", component: () => import('pages/AdventuresView.vue'), props: true}
    ]
  },
  {
    path: '/user',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', name: "UserPage", component: () => import('pages/UserPage.vue'), meta: { requires_auth: true } }
    ]
  },
  {
    path: '/admin',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', name: "AdminAdventures", component: () => import('pages/AdminView.vue'), meta: { requires_admin: true } }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
]

export default routes
