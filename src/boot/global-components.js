import {defineAsyncComponent} from "vue"

export default async ({ app }) => {
  app.component('adventures-create', defineAsyncComponent(() => import('../components/AdventuresCreate.vue')))
  app.component('adventures-list', defineAsyncComponent(() => import('../components/AdventuresList.vue')))
  app.component('adventure-card', defineAsyncComponent(() => import('../components/adventures/AdventureCard.vue')))
  app.component('adventure-item', defineAsyncComponent(() => import('../components/adventures/AdventureItem.vue')))
  app.component('quest-view', defineAsyncComponent(() => import('../components/QuestView.vue')))
  app.component('mark-down', defineAsyncComponent(() => import('../components/MarkDown.vue')))
  app.component('user-avatar', defineAsyncComponent(() => import('../components/UserAvatar.vue')))
  app.component('step-item', defineAsyncComponent(() => import('../components/adventures/StepItem.vue')))
  // Parts
  app.component('xp-flag', defineAsyncComponent(() => import('../components/parts/XPFlag.vue')))
  app.component('corner-ribbon', defineAsyncComponent(() => import('../components/parts/CornerRibbon.vue')))
  app.component('stat-chip', defineAsyncComponent(() => import('../components/parts/StatChip.vue')))
  app.component('tool-tip', defineAsyncComponent(() => import('../components/parts/ToolTip.vue')))
}
